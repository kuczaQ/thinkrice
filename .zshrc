#!/bin/zsh

###########################
# General ZSH  configuration
###########################

    # Path to your oh-my-zsh installation
    export ZSH="/home/adam/.oh-my-zsh"

    # Adding ruby to PATH
    export PATH=$PATH:$HOME/.gem/ruby/2.6.0/bin

    # I have no idea what this line does...
    export GPG_TTY=$(tty)

    # Set name of the theme to load 
    ZSH_THEME="robbyrussell"

    # Command auto-correction
    ENABLE_CORRECTION="true"

    # Red dots whilst waiting for completion
    COMPLETION_WAITING_DOTS="true"

    # Which plugins would you like to load?
    plugins=(
        z
        zsh-syntax-highlighting
    )

    source $ZSH/oh-my-zsh.sh


    # The following lines were added by compinstall
    zstyle ':completion:*' auto-description 'specify: %d'
    zstyle ':completion:*' completer _expand _complete _ignored _match _correct _approximate _prefix
    zstyle ':completion:*' format 'Completing %d'
    zstyle ':completion:*' group-name ''
    zstyle ':completion:*' insert-unambiguous true
    zstyle ':completion:*' list-colors ''
    zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
    zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}'
    zstyle ':completion:*' max-errors 3
    zstyle ':completion:*' menu select=long
    zstyle ':completion:*' original true
    zstyle ':completion:*' prompt 'Corrections: %e'
    zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
    zstyle ':completion:*' verbose true
    zstyle :compinstall filename '/home/adam/.zshrc'

    autoload -Uz compinit
    compinit
    # End of lines added by compinstall
###########################
###########################



###########################
# Shell configuration
###########################

    # Use vim keys in tab complete menu:
    bindkey -M menuselect 'h' vi-backward-char
    bindkey -M menuselect 'k' vi-up-line-or-history
    bindkey -M menuselect 'l' vi-forward-char
    bindkey -M menuselect 'j' vi-down-line-or-history

    # Use lf to switch directories and bind it to ctrl-o
    lfcd () {
        tmp="$(mktemp)"
        lf -last-dir-path="$tmp" "$@"
        if [ -f "$tmp" ]; then
            dir="$(cat "$tmp")"
            rm -f "$tmp"
            [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
        fi
    }
    # Bind CTRL + o to lfcd
    bindkey -s '^o' 'lfcd\n'

    # Show hidden files in tab completion
    setopt globdots

    # Lines configured by zsh-newuser-install
    HISTFILE=~/.histfile
    HISTSIZE=10000
    SAVEHIST=10000
    setopt appendhistory autocd
    unsetopt beep
    # End of lines configured by zsh-newuser-install

    # Prompt
    parse_git_branch () {
        git branch 2> /dev/null | grep "*" | sed -e 's/* \(.*\)/ (\1)/g'
    }
    YELLOW=$'\033[38;5;228m'

    PROMPT='%B%F{red}%n%f 💖  %F{magenta}%m%f%b %F{yellow}%1~%f $ '
    RPROMPT='%F{yellow}$(parse_git_branch) %f%{$BLACK%}[%F{yellow}%?%f]'

    bindkey "^[[A" up-line-or-search    # Up
    bindkey "^[[B" down-line-or-search  # Down
###########################
###########################


###########################
# General aliases & functions
###########################

    # Adding color
    alias grep="grep --color=auto"
    alias diff="diff --color=auto"
    alias ccat="highlight --out-format=ansi" # Color cat - print file with syntax highlighting
    alias ls='ls -hNl --color=auto --group-directories-first'

    # Utility aliases
    alias p="sudo pacman"
    alias ll="ls -la"
    alias del="sudo rm -r"

    alias i="sudo pacman -S --noconfirm"
    alias sleep="sudo systemctl suspend"
    alias xclip='xclip -selection clipboard'

    # Personal aliases
    alias downboi="sudo shutdown now"
    alias ref="shortcuts && source ~/.zshrc" # Refresh shortcuts manually and reload zshrc
    alias battery1='cat /sys/class/power_supply/BAT0/capacity'

    # Screen management
    alias ventum="~/.screenlayout/ventum_screens.sh" 

    setBrightness() {
        passedVal=$1
        maxBrightness=$(cat /sys/class/backlight/intel_backlight/max_brightness)
        if [ $passedVal -gt 100  ]; then
            passedVal=100
        fi

        currBrightness=$((($passedVal*$maxBrightness)/100))

        sudo bash -c "echo $currBrightness > /sys/class/backlight/intel_backlight/brightness"
    }
    alias brrt="setBrightness"

    # Work aliases
    alias kurwa="cd ~/Work/SWMH/iiqrepo; ll"
    alias svpn="sudo openconnect -u MazurkiewiA --passwd-on-stdin --authgroup 2  vpn.swmh.de"
    alias sshqa="ssh idmadmin@swmhidmqa.swmh.intern"
    alias sshprod1="ssh idmadmin@swmhidmapp1.idm.swmh.intern"
    alias sshprod2="ssh idmadmin@swmhidmapp2.idm.swmh.intern"


    # Project aliases
    alias compUp="sudo docker-compose up -d"
    alias compDown="sudo docker-compose down"
    alias compRestart="sudo docker-compose restart"

    # XMAS
    alias rebuild="sudo docker-compose build && sudo docker-compose up -d && sudo docker logs -f xmas-app"
    alias logXmas="sudo docker logs -f xmas-app"
###########################
###########################



###########################
# Git aliases and tools
###########################

    alias stat="git status"

    ## Updates the repo with the modified changes
    update() {
        git status | sed '/Untracked files:/q' | head -n-1
        
        if [ -z $1 ]; then
            printf "You need to provide a commit message: " && read -r MSG
        else
            MSG=$1
        fi


        printf "Add all modified? [y/N] " && read REPLY
        [[ $REPLY =~ ^[Yy]$ ]] && git add -u

        git diff --cached --stat | tail -n1

        while : ; do
            printf "Are you sure? (p prints the list) [Y/n] " && read REPLY
            [[ $REPLY =~ ^[Pp]+$ ]] && git status || break
        done
        [[ $REPLY =~ ^[Yy]?$ ]] && git commit -m "$MSG" || return

        printf "Push? [Y/n] " && read REPLY
        [[ $REPLY =~ ^[Yy]?$ ]] && git push
    }

    ## Fetch all the remote branches and creates local branches
    fetchAll() {
        for remote in `git branch -r`; do git branch --track ${remote#origin/} $remote; done
    }

    ## Add a single file to the tracked files
    add() {
        git add $1
    }

    ## Add a single file to .gitignore
    ignore() {
        echo $1 >> .gitignore
    }
###########################
###########################



## Load shortcut aliases
[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc"


################################################
#   Old options added by oh-my-zsh and Luke
################################################ 

# Uncomment the following line to use case-sensitive completion
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion
# Case-sensitive completion must be off. _ and - will be interchangeable
# HYPHEN_INSENSITIVE="true"

# autoload -U up-line-or-beginning-search
# autoload -U down-line-or-beginning-search
# zle -N up-line-or-beginning-search
# zle -N down-line-or-beginning-search
